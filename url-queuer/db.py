
import pyodbc
import requests, json


def dbConnect():
	server = "tcp:paybyanswer.com\ROBERTDELLSQL,1002"
	database = "Queue"
	username = "Queue"
	password = "!QueueManager"
	_connection = "DRIVER={ODBC Driver 17 for SQL Server};SERVER="+server+";DATABASE="+database+";UID="+username+";PWD="+ password+";Trusted_Connection=no"
	cnxn = pyodbc.connect(_connection, timeout=9000)
	# cursor = cnxn.cursor()

	return cnxn



def couchCRUD(param, type="GET"):
	couchURI = "http://admin:Scr4p1ng@2018@206.189.153.57:5984"

	# GET DOC
	if type.lower() == "get":
		docURI = couchURI + "/" + param["db"] + "/_design/queue/_view/missions?descending=true&include_docs=true"

		if "limit" in param:
			docURI = docURI + "&limit=" + str(param["limit"])

		req = requests.get(docURI, headers={"Content-type": "application/json"})
		_rows = json.loads(req.text)["rows"]

		rows = []
		for nth in _rows:
			if "_design/" not in nth["id"]:
				rows.append(nth["doc"])

		return rows

	# UPSERT DOC
	elif type.lower() == "upsert":
		docURI = couchURI + "/" + param["db"] + "/" + param["_id"]
		requests.put(docURI, headers={"Content-type": "application/json"}, data=json.dumps(param["doc"]))

	return None




# NOTES
# http://206.189.153.57:5984/_utils/#database/queue_amazondetailpage/_design%2Fqueue
# {
#   "_id": "_design/queue",
#   "_rev": "1-4f76d05b07d25645a7c77332f81fbfb2",
#   "views": {
#     "open": {
#       "map": "function (doc) {\n  if(doc.Priority == 1){ emit(doc._id, doc); }\n}"
#     },
#     "missions": {
#       "map": "function (doc) {\n  emit(doc.CreateDate, doc);\n}"
#     }
#   },
#   "language": "javascript"
# }

# http://206.189.153.57:5984/_utils/#database/queue_amazonsearchterm/_design%2Fqueue
# {
#   "_id": "_design/queue",
#   "_rev": "7-771dd795aede2a03396d837bc6a8cccb",
#   "views": {
#     "open": {
#       "map": "function (doc) {\n  if(doc.Priority == 1){ emit(doc._id, doc); }\n}"
#     },
#     "missions": {
#       "map": "function (doc) {\n  emit(doc.CreateDate, doc);\n}"
#     }
#   },
#   "language": "javascript"
# }