

# STANDALONE REST API
import datetime
from flask import Flask, request, jsonify
from flask_cors import CORS

from db import dbConnect, couchCRUD


app = Flask(__name__)
app.config["SECRET_KEY"] = "secret!"
CORS(app)


@app.route("/", methods=["GET"])
def ping():
	response = { "data": None, "message": "QUEUE: Alive and kick'n!" }
	return (jsonify(response), 200)



def getQueue():
	try:

		queue = []
		rows = couchCRUD({ "db": "queue_amazonsearchterm", "limit": 50 }, "GET")

		for row in rows:
			try:
				queue.append({
					"status": row["Priority"],
					"term": row["SearchTerm"],
					"created": row["CreateDate"],
				})
			except:
				pass

	except Exception as err:
		print("RETRIEVE-ERR: " + str(err))

	return queue




@app.route("/queue/add",  methods=["POST"])
def queue_add():
	data = request.json
	newQueue = []

	try:

		# cnxn = dbConnect()
		# cursor = cnxn.cursor()
		# cursor.execute("INSERT INTO QUEUE_AmazonSearchTerm (SearchTerm, Priority) VALUES ('%s', 1);"%data["term"])
		# cnxn.commit()

		now = datetime.datetime.utcnow().isoformat()
		_id = now.split("T")[0] + ":[" + data["term"] + "]"
		tmp = couchCRUD({
			"db": "queue_amazonsearchterm",
			"_id": _id,
			"doc": {
				"Priority": 1,
				"CreateDate": now,
				"SearchTerm": data["term"],
			}
		}, "UPSERT")

		newQueue = getQueue()

	except Exception as err:
		print("COMMIT-ERR: " + str(err))

	return jsonify({ "results": newQueue }), 200




@app.route("/queue",  methods=["GET", "POST"])
def queue():
	return jsonify({ "results": getQueue() }), 200




if __name__ == "__main__":

	app.run(host="0.0.0.0", port=5000, debug=True, threaded=True)


	# gunicorn --workers=1 --bind=0.0.0.0:5000 --log-level=debug --log-file=logs.log --reload --daemon  --timeout=9000 --enable-stdio-inheritance api:app