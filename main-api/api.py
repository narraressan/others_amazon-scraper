

# STANDALONE REST API
import datetime
from flask import Flask, request, jsonify
from flask_cors import CORS

from db import couchCRUD


app = Flask(__name__)
app.config["SECRET_KEY"] = "secret!"
CORS(app)


@app.route("/", methods=["GET"])
def ping():
	response = { "data": None, "message": "API: Alive and kick'n!" }
	return (jsonify(response), 200)



def getUser(email):
	try:

		user = {}
		row = couchCRUD({ "db": "user_registry", "email": email }, "GET")

		try:
			user = row
		except:
			pass

	except Exception as err:
		print("RETRIEVE-ERR: " + str(err))

	return user




@app.route("/signup",  methods=["POST"])
def queue_add():
	data = request.json
	user = {}

	try:

		now = datetime.datetime.utcnow().isoformat()
		_id = data["email"].strip()
		tmp = couchCRUD({
			"db": "user_registry",
			"_id": _id,
			"doc": {
				"email": data["email"].strip(),
				"password": data["password"].strip(),
				"fullname": data["fullname"].strip(),
				"address": data["address"].strip(),
				"created": now
			}
		}, "UPSERT")

		user = getUser(data["email"].strip())

	except Exception as err:
		print("COMMIT-ERR: " + str(err))

	return jsonify({ "user": user }), 200




@app.route("/login",  methods=["POST"])
def queue():
	data = request.json

	user = {}
	tmp = getUser(data["email"])
	if "email" in tmp and "password" in tmp:
		if tmp["password"] == data["password"]:
			user = tmp

	return jsonify({ "user": user }), 200




if __name__ == "__main__":

	app.run(host="0.0.0.0", port=2000, debug=True, threaded=True)


	# gunicorn --workers=1 --bind=0.0.0.0:2000 --log-level=debug --log-file=logs.log --reload --daemon  --timeout=9000 --enable-stdio-inheritance api:app