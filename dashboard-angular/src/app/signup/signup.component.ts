import { Component, OnInit } from '@angular/core';
import { FactoryService } from '../_services/factory.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [FactoryService],
})
export class SignupComponent implements OnInit {

	email: string = "";
	password: string = "";
	fullname: string = "";
	address: string = "";
	disabled: boolean = false;
	factory: FactoryService;
	router: Router;

	constructor(private service: FactoryService, private route: Router) {
		this.factory = service;
		this.router = route;
	}

	ngOnInit() {
		console.log("Init: signup");
	}

	_signup(): void {
		if(this.email.trim() != "" && this.password != "" && this.fullname != "" && this.address != ""){
			this.disabled = true;
			var user = {
				email: this.email,
				password: this.password,
				fullname: this.fullname,
				address: this.address,
			};

			this.factory.signupUser(user, this._onSuccess, this._onError, this);
		}
		else{ this.factory.notify("Don't leave an empty field", "error"); }
	}

	_onError(error): void { console.log(error); }

	_onSuccess(data, scope): void {
		console.log(data["user"]);
		scope.disabled = false;

		scope.router.navigate(["/login"]);
	};
}
