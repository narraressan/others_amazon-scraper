import { Component, OnInit } from '@angular/core';
import { FactoryService } from '../_services/factory.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [FactoryService],
})
export class LoginComponent implements OnInit {

	email: string = "";
	password: string = "";
	disabled: boolean = false;
	factory: FactoryService;
	router: Router;

	constructor(private service: FactoryService, private route: Router) {
		this.factory = service;
		this.router = route;
	}

	ngOnInit() {
		console.log("Init: login");
	}

	_login(): void {
		if(this.email.trim() != "" && this.password != ""){
			this.disabled = true;
			var user = {
				email: this.email,
				password: this.password
			};

			this.factory.loginUser(user, this._onSuccess, this._onError, this);
		}
		else{ this.factory.notify("Don't leave an empty field", "error"); }
	}

	_onError(error): void { console.log(error); }

	_onSuccess(data, scope): void {
		console.log(data["user"]);
		scope.disabled = false;

		var email = data["user"]["email"];
		if(email){ scope.router.navigate(["/dashboard/" + email]); }
	};

}
