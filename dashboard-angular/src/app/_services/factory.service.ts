import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import notie from 'notie';
import NProgress from 'nprogress';


// <style type="text/css" src="notie/dist/notie.min.css"></style>
		// <style type="text/css" src="nprogress/nprogress.css"></style>

@Injectable({
	providedIn: 'root'
})

export class FactoryService implements OnInit {

	host: string = "http://206.189.153.57";
	// host: string = "http://localhost";
	queueHost: string = this.host + ":5000";
	apiHost: string = this.host + ":2000";

	constructor(private http: HttpClient) { }


	ngOnInit() {
		console.log("Init: factoryservice");
	}


	notify(message, type="info"): void {
		notie.alert({
			type: type, // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
			text: message,
			time: 5, // optional, default = 3, minimum = 1,
			position: "bottom" // optional, default = 'top', enum: ['top', 'bottom']
		})
	};


	load(): void { NProgress.start(); };


	unload(): void { NProgress.done(); };


	request(options={}): void {
		this.load();

		var url = (options["url"]) ? options["url"] : "",
				body = (options["body"]) ? options["body"] : {},
				onSuccess = (options["onSuccess"]) ? options["onSuccess"] : null,
				onError = (options["onError"]) ? options["onError"] : null,
				scope = (options["scope"]) ? options["scope"] : null;

		const httpOptions = {
			headers: new HttpHeaders({ 'Content-Type':  'application/json', })
		};

		var self = this;

		this.http.post(url, body, httpOptions)
		.subscribe(
			res => {
				if(onSuccess != null){ onSuccess(res, scope); }
				self.unload();
			},
			err => {
				self.unload();
				self.notify("Oops! Something went wrong.", "error");
				if(onError != null){ onError(err); }
			}
		);
	};


	addQueue(data, onSuccess, onError, scope): void {
		var self = this;
		this.request({
			method: "POST",
			url: this.queueHost + "/queue/add",
			body: data,
			onSuccess: onSuccess,
			onError: onError,
			scope: scope,
		});
	};


	getQueue(onSuccess, onError, scope): void {
		var self = this;
		this.request({
			method: "GET",
			url: this.queueHost + "/queue",
			onSuccess: onSuccess,
			onError: onError,
			scope: scope,
		});
	};

	signupUser(data, onSuccess, onError, scope): void {
		var self = this;
		this.request({
			method: "POST",
			url: this.apiHost + "/signup",
			body: data,
			onSuccess: onSuccess,
			onError: onError,
			scope: scope,
		});
	};

	loginUser(data, onSuccess, onError, scope): void {
		var self = this;
		this.request({
			method: "POST",
			url: this.apiHost + "/login",
			body: data,
			onSuccess: onSuccess,
			onError: onError,
			scope: scope,
		});
	};
}
