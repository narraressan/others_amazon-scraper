import { Component, OnInit, Input, SimpleChanges } from "@angular/core";

@Component({
	selector: "app-navbar",
	templateUrl: "./navbar.component.html",
	styleUrls: ["./navbar.component.css"]
})

export class NavbarComponent implements OnInit {

	@Input() isLoading: string;

	constructor() {}

	ngOnInit() {
		console.log("Init: navbar");
	}


	// https://angularfirebase.com/lessons/sharing-data-between-angular-components-four-methods/
	ngOnChanges(changes: SimpleChanges) {
		console.log(changes);
	}
}
