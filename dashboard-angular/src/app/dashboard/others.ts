import { Component } from '@angular/core';
import { FactoryService } from '../_services/factory.service';

@Component({
	providers: [FactoryService],
})


class Queuer {

	searchTerm: string = "";
	results: object[] = [];

	loadingState: boolean = false;
	factory: FactoryService = null;


	constructor(public service: FactoryService){
		this.factory = service;
	}



	_addQueue(): void {
		if(this.searchTerm.trim() != ""){
			var search = { term: this.searchTerm };

			this.searchTerm = "";
			this.loadingState = true;
			this.factory.addQueue(search, this._onSuccess, this._onError, this);
		}
		else{ this.factory.notify("Please provide a valid search term", "error"); }
	};



	_getQueue(): void {
		this.loadingState = true;
		this.factory.getQueue(this._onSuccess, this._onError, this);
	};



	_onError(error): void { console.log(error); }


	_onSuccess(data, scope): void {
		// console.log(data["results"]);
		scope.results = data["results"];
		scope.loadingState = false;
		scope.factory.notify(data["results"].length + " results", "success");
	};

}



export { Queuer }