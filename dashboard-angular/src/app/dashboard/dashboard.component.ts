import { Component, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Queuer } from './others';
import { FactoryService } from '../_services/factory.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css'],
	providers: [FactoryService],
})
export class DashboardComponent extends Queuer implements OnInit {

	constructor(public service: FactoryService, private route: ActivatedRoute){
		super(service);

		this.route.params.subscribe(res => console.log(res.id));
	}


	ngOnInit() {
		console.log("Init: dashboard");

		this._getQueue();
	}


	ngOnChanges(changes: SimpleChanges) {
		console.log(changes);
	}

}