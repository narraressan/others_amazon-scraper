

import re
from bs4 import BeautifulSoup

try:
	import urlparse as parse
except:
	from urllib import parse

class AmazonCom_Parser:

	raw = None
	soup = None

	def __init__(self, html):
		try:
			self.raw = html
			self.soup = BeautifulSoup(html, "html.parser")
		except Exception as err0:
			print("PARSE-ERR0: " + str(err0))
			# print(self.raw)


	def parse_searchPage(self, search):
		# get all links to info page
		if self.soup == None:
			return []

		results = self.soup.select("li.s-result-item.celwidget")
		parsed = []

		for result in results:
			meta = {}
			meta["search_term"] = search

			try:
				meta["url"] = result.select("div > a.s-access-detail-page")[0].attrs["href"]

				# handle possible page redirect
				if "/gp/slredirect/picassoRedirect.html" in meta["url"]:
					meta["url"] = parse.unquote(meta["url"].split("&url=")[1])

				meta["url"] = meta["url"].split("/ref=")[0]
			except Exception as err:
				print("URL-ERR: " + str(err))
				meta["url"] = ""
				pass

			try:
				meta["title"] = result.select("a.s-access-detail-page")[0].attrs["title"].encode("utf8", "ignore")
			except Exception as err:
				print("TITLE-ERR: " + str(err))
				meta["title"] = ""
				pass

			try:
				tmp = result.select("a > span.a-offscreen")[0].getText().replace("$", "").replace(",", "").replace(" ", "").strip()
				meta["price"] = float(tmp)
			except Exception as err:
				print("PRICE-ERR: " + str(err))
				meta["price"] = 0
				pass

			try:
				meta["img"] = result.select("img")[0].attrs["src"]
			except Exception as err:
				print("IMG-ERR: " + str(err))
				meta["img"] = ""
				pass

			try:
				tmp = result.select("i.a-icon.a-icon-star")[0].getText().split("out of")[0].strip()
				meta["rating"] = float(tmp)
			except Exception as err:
				print("RATING-ERR: " + str(err))
				meta["rating"] = 0
				pass

			try:
				meta["is_sponsored"] = False
				if len(result.select("h2 > span.a-offscreen")) > 0:
					meta["is_sponsored"] = True
			except Exception as err:
				print("SPONSORED-ERR: " + str(err))
				meta["is_sponsored"] = False
				pass

			try:
				meta["is_prime"] = False
				if len(result.select("i[aria-label='Prime']")) > 0:
					meta["is_prime"] = True
			except Exception as err:
				print("PRIME-ERR: " + str(err))
				meta["is_prime"] = False
				pass

			try:
				meta["seller"] = result.select("div.a-row.a-spacing-none > span.a-size-small.a-color-secondary")[1].getText()
			except Exception as err:
				print("SELLER-ERR: " + str(err))
				meta["seller"] = ""
				pass

			try:
				tmp = result.select("div.a-row.a-spacing-top-mini.a-spacing-none > a.a-size-small.a-link-normal.a-text-normal")[0].getText().replace(",", "").replace(" ", "").strip()
				meta["reviews_count"] = int(tmp)
			except Exception as err:
				print("REVIEWS-ERR: " + str(err))
				meta["reviews_count"] = 0
				pass

			parsed.append(meta)

		return parsed


	def parse_infoPage(self):
		infoRaw = []
		infoParsed = {
			"Title": None,
			"Reviews": None,
			"Price": None,
			"Shipping": None,
			"Prime": None,
			"InStock": None,
			"ImageURL": None,
			"NumberOfSellers": None,
			"BuyBoxSeller": None,
			"MostRecentReviews": None,
			"BulletPoint": [],
			"Description": None,
			"FBT_URL": [],
		}
		try:
			if self.soup == None:
				return None




			# Possible location of uable content
			# Location 1 & 2
			infoRaw.extend(self.soup.select("div.content > ul > li"))
			infoRaw.extend(self.soup.select("div#feature-bullets > ul > li"))
			for info in infoRaw:
				try:
					infoParsed["BulletPoint"].append(info.getText().strip())
				except Exception as err1:
					print("PARSE-ERR1: " + str(err1))
					pass

			# Location 3
			infoRaw = self.soup.select("div#productDescription > p")
			for info in infoRaw:
				try:
					infoParsed["BulletPoint"].append(self.striphtml(str(info)))
				except Exception as err2:
					print("PARSE-ERR2: " + str(err2))
					pass


			try:
				infoParsed["Title"] = self.soup.select("span#productTitle")[0].getText().strip()
			except Exception as err:
				print("TITLE-ERR: " + str(err))
				pass


			try:
				infoParsed["Reviews"] = self.soup.select("span#acrPopover")[0].attrs["title"]
			except Exception as err:
				print("REVIEWS-ERR: " + str(err))
				pass


			try:
				infoParsed["Price"] = self.soup.select("span#priceblock_ourprice")[0].getText().strip()
			except Exception as err:
				print("PRICE-ERR: " + str(err))
				pass


			try:
				infoParsed["Shipping"] = self.soup.select("span#ourprice_shippingmessage")[0].getText().strip()
			except Exception as err:
				print("SHIPPING-ERR: " + str(err))
				pass


			try:
				infoParsed["Prime"] = True if len(self.soup.select("div#primeExclusiveBuyBox_feature_div")) > 0 else False
			except Exception as err:
				print("PRIME-ERR: " + str(err))
				pass


			try:
				if self.soup.select("div#availability")[0].getText().strip().lower() == "In Stock":
					infoParsed["InStock"] = True
				else:
					infoParsed["InStock"] = False
			except Exception as err:
				print("INSTOCK-ERR: " + str(err))
				pass


			try:
				infoParsed["ImageURL"] = self.soup.select("div#imgTagWrapperId img")[0].attrs["src"]
			except Exception as err:
				print("IMAGEURL-ERR: " + str(err))
				pass


			try:
				tmp = self.soup.select("div#moreBuyingChoices_feature_div > div > div > div > div > div > div > span")[0].getText().strip()
				_vals = re.findall("\((.*?)\)", tmp)
				infoParsed["NumberOfSellers"] = int(_vals[0])
			except Exception as err:
				print("NUMBEROFSELLERS-ERR: " + str(err))
				pass


			try:
				infoParsed["BuyBoxSeller"] = self.soup.select("div#shipsFromSoldBy_feature_div")[0].getText().strip()
			except Exception as err:
				print("BYBOXSELLER-ERR: " + str(err))
				pass


			try:
				name = self.soup.select("div#most-recent-reviews-content > div.a-section.review span.a-profile-name")[0].getText().strip()
				comment = self.soup.select("div#most-recent-reviews-content > div.a-section.review > a.a-link-normal.a-text-normal")[0].getText().strip()
				infoParsed["MostRecentReviews"] = name + ": " + comment
			except Exception as err:
				print("MOSTRECEBTREVIEWS-ERR: " + str(err))
				pass


			try:
				infoParsed["Description"] = self.soup.select("meta[name='description']")[0].attrs["content"]
			except Exception as err:
				print("MOSTRECEBTREVIEWS-ERR: " + str(err))
				pass


			# WHAT IS FBT_URL



			return infoParsed
		except Exception as err3:
			print("PARSE-ERR3: " + str(err3))

		return infoParsed


	def striphtml(self, data):
		p = re.compile(r"<.*?>")
		return p.sub(u"\n", data)