

import requests, time, json, validators


class AmazonCom_Downloader:

	proxy = None


	def __init__(self, proxy):
		self.proxy = proxy



	def request_splash(self, url, tries = 1):
		print("Downloading: " + str(url))
		if validators.url(url):
			retry = tries
			try:
				# APPLICABLE ONLY TO SITES THAT DO NOT ACTIVELY BLOCK PEOPLE

				lua_source = """
				function use_crawlera(splash)
					-- Put your Crawlera username and password here. This is different from your
					-- Scrappinghub account. Find your Crawlera username and password in
					-- https://app.scrapinghub.com/

					local host = "{{HOST}}"
					local port = "{{PORT}}"
					local session_header = "X-Crawlera-Session"
					local session_id = "create"

					splash:on_request(function (request)
						-- Requests to Google domains are not allowed by Crawlera, but pages
						-- frequently include tracking code or ads served by google. Block
						-- those requests.
						--
						-- lua patterns follow a different syntax to normal regular expressions
						if string.find(request.url, 'google%.[a-z][a-z][a-z]?') or
						   string.find(request.url, 'doubleclick%.net') or
						   string.find(request.url, 'googleapis%.com') then
							request.abort()
							return
						end

						-- If possible, avoid using Crawlera for subresource requests that
						-- are not monitored. This will increase speed a lot. Here are some
						-- example rules that you can use to match subresources:

						-- Don't use Crawlera for domains starting with static.
						if string.find(request.url, '://static%.') ~= nil then
							return
						end

						-- Don't use Crawlera for urls ending in .png
						if string.find(request.url, '%.png$') ~= nil then
							return
						end
						-- request:set_header("X-Crawlera-UA", "desktop")
						request:set_header('X-Crawlera-Timeout', 40000)
						request:set_header('X-Crawlera-Cookies', 'disable')
						request:set_header(session_header, session_id)
						request:set_proxy{host=host, port=port}
					end)

					splash:on_response_headers(function (response)
						if type(response.headers[session_header]) ~= nil then
							session_id = response.headers[session_header]
						end
					end)
				end

				function main(splash)
					use_crawlera(splash)

					assert(splash:go(splash.args.url))
					return splash:html()
				end
				"""

				source = lua_source.replace('{{POST}}',self.proxy.split(":")[0]).replace('{{HOST}}',self.proxy.split(":")[1])
				data = {
					'url': url,
					'lua_source': source
				}

				# point to where the splash is running
				req = requests.post("http://206.189.153.57:8050/execute", headers = {'Content-Type': 'application/json'}, data = json.dumps(data))
				return req.text.encode("utf8", "ignore")

			except Exception as err:
				print("DL-ERR: " + str(err) + " -- " + str(retry) + " retry for [" + str(url) + "] in few seconds")

				time.sleep(2)
				retry = retry + 1
				if retry >= 3: # limited retries
					return None
				else:
					# retry
					self.request_splash(url, retry)
		else:
			print("INVALID-URL: " + str(url))

		return False

