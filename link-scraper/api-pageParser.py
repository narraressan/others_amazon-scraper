

import time, random, datetime
from flask import Flask, jsonify, request
from flask_cors import CORS
from multiprocessing import Process, Manager

from amazon_com_parser import AmazonCom_Parser
from amazon_com_downloader import AmazonCom_Downloader

from db import dbConnect, couchCRUD

app = Flask(__name__)
app.config["SECRET_KEY"] = "secret!"
CORS(app)



@app.route("/", methods=["GET"])
def ping():
	response = { "data": None, "message": "PAGE-SCRAPER: Alive and kick'n!" }
	return (jsonify(response), 200)



def getPriority():
	try:

		queue = couchCRUD({ "db": "queue_amazondetailpage", "limit": 5 }, "GET")

	except Exception as err:
		print("RETRIEVE-ERR: " + str(err))
		return []

	return queue



def completePriority(mission):
	try:

		# cnxn = dbConnect()
		# cursor = cnxn.cursor()

		# # 0 means this search term has been executed
		# cursor.execute("UPDATE QUEUE_AmazonSearchTerm SET Priority=0 WHERE Id=%s"%mission["id"])
		# cnxn.commit()

		doc = mission
		doc["Priority"] = 0
		print("COMPLETING: " + doc["_id"])
		couchCRUD({
			"db": "queue_amazondetailpage",
			"_id": doc["_id"],
			"doc": doc
		}, "UPSERT")

	except Exception as err:
		print("COMPLETE-ERR: " + str(err))



def getBullets(mission, browser):
	raw = browser.request_splash("https://www.amazon.com/dp/" + mission["ASIN"])
	info = AmazonCom_Parser(raw).parse_infoPage()
	# print(info)

	try:

		now = datetime.datetime.utcnow().isoformat()
		_id = now.split("T")[0] + ":[" + mission["ASIN"] + "]"
		print("SAVING: " + _id)
		info["PageURL"] = mission["ProductURL"]
		info["ASIN"] = mission["ASIN"]
		info["DateCreated"] = now

		couchCRUD({
			"db": "queue_amazondetailpage_results",
			"_id": _id,
			"doc": info
		}, "UPSERT")

	except Exception as err:
		print("SAVE-INFO-ERR: " + str(err))
		print(info)



def computeASIN(url):
	try:
		_url = url.split("/")
		# ASIN is commonly 10 characters long
		_ASIN = _url[len(_url)-1]
		if len(_ASIN) == 10:
			return _ASIN
	except:
		return None



def startMonitor(flag=False):

	proxy = "37.48.118.90:13042"

	while(flag):
		try:
			browser = AmazonCom_Downloader(proxy)
			queue = getPriority()

			if len(queue) > 0:
				mission = random.choice(queue)

				getBullets(mission, browser)

				# flag the Priority = 0 as complete mission
				completePriority(mission)

		except Exception as err:
			print("ERR: " + str(err))

		# random resting time before querying again in the database
		rest = random.randint(10, 30)
		print("REST: " + str(rest))
		time.sleep(rest)



if __name__ == '__main__':

	# nohup python -u api-pageParser.py > logs_pageParse.log &
	startMonitor(True)

	# process = Process(target=startMonitor, args=(True,))
	# process.daemon = True
	# process.start()

	# app.debug = True
	# app.host = "0.0.0.0"
	# app.port = 4000
	# app.threaded = True
	# app.run()

	# gunicorn --workers=1 --bind=0.0.0.0:4000 --log-level=debug --log-file=logs.log --reload --daemon  --timeout=9000 --enable-stdio-inheritance api:app